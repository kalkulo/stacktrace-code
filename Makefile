# Use GNU C++ compiler
CC=g++
# Compiler settings (warning on all)
CFLAGS=-Wall -I. -g --std=c++11
OBJDIR := obj

all: test_code libstacktrace.so

test_code:  $(OBJDIR) obj/call_stack_gcc.o obj/test_code.o
	$(CC) obj/test_code.o obj/call_stack_gcc.o -ldl -lunwind -rdynamic -o test_code

obj/call_stack_gcc.o:
	$(CC) $(CFLAGS) -c stacktrace/call_stack_gcc.cpp -o obj/call_stack_gcc.o

obj/test_code.o:
	$(CC) $(CFLAGS) -c test/test_code.cpp -o obj/test_code.o

$(OBJDIR): FORCE
	mkdir -p obj

FORCE:

libstacktrace.so: stacktrace/call_stack_gcc.cpp
	$(CC) $(CFLAGS) -shared -fPIC $< -o $@ -lunwind

clean:
	rm -rf obj/*.o test_code lib/*.so
