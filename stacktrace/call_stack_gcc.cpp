/* Copyright (c) 2009, Fredrik Orderud
   License: BSD licence (http://www.opensource.org/licenses/bsd-license.php)
   Based on: http://stupefydeveloper.blogspot.com/2008/10/cc-call-stack.html */

/* Liux/gcc implementation of the call_stack class. */

#include <cstddef>
#ifdef __GLIBCXX__

#include <stdio.h>
#include <execinfo.h>
#include <cxxabi.h>
#include <dlfcn.h>
#include <stdlib.h>

#define UNW_LOCAL_ONLY
#include <libunwind.h>

#include "call_stack.hpp"

#define MAX_DEPTH 32

namespace {

bool fill_in_entry(void *addr, stacktrace::entry& e)
{
    Dl_info dlinfo;
    if (!dladdr(addr, &dlinfo))
        return false;

    if (!dlinfo.dli_fname || !dlinfo.dli_sname)
        return false;

    void *offset;
    if (dlinfo.dli_fbase == (void*)0x400000)
        offset = addr;
    else
        offset = (void*)((char*)addr-(char*)dlinfo.dli_fbase);

    FILE* f = nullptr;
    {
      char buf[256];
      snprintf(buf, sizeof(buf), "/usr/bin/addr2line -ifCe %s %p", dlinfo.dli_fname, offset);
      //printf("%s\n", buf);
      f = popen(buf, "r"); // popen is slow, perhaps use popen_nosh or library
    }
    bool found = false;
    while (f) {
        // Note that addr2line with "-i" flag may return multiple results, use the last one
        char symbol[256], filename[256];
        size_t line;
        if (fscanf(f, "%255[^\n]\n", symbol) != 1)
            break;
        if (fscanf(f, "%255[^:]:%lu\n", filename, &line) != 2)
            break;
        //if (filename[0] == '?' || symbol[0] == '?')
        //    continue;
        e.file     = filename;
        e.line     = line;
        e.function = symbol;
        found = true;
    }

    if (!found) {
        int    status;
        char * demangled = abi::__cxa_demangle(dlinfo.dli_sname, nullptr, nullptr, &status);
        e.file     = dlinfo.dli_fname;
        e.line     = 0;
        e.function = (demangled ? demangled : dlinfo.dli_sname);

        free(demangled);
    }

    return true;
}
}

namespace stacktrace {

call_stack::call_stack() {

    unw_cursor_t    cursor;
    unw_context_t   context;

    unw_getcontext(&context);
    unw_init_local(&cursor, &context);

    do {
        if (unw_is_signal_frame(&cursor) > 0) {
            // Signal frame detected: Restart walk from here
            trace.clear();
        } else {
            trace.push_back(nullptr);
            unw_get_reg(&cursor, UNW_REG_IP, (unw_word_t*)&trace.back());
        }
    }  while (trace.size() < MAX_DEPTH && unw_step(&cursor) > 0);
}

std::vector<entry> call_stack::resolve_stack() const {
    std::vector<entry> stack;
    // lookup symbols
    entry e;
    for (size_t i=0; i<trace.size(); i++) {
        if (fill_in_entry(trace[i], e))
            stack.push_back(e);
    }
    return stack;
}

call_stack::~call_stack () throw() {
}

} // namespace stacktrace

#endif // __GNUC__
