/* Copyright (c) 2009, Fredrik Orderud
   License: BSD licence (http://www.opensource.org/licenses/bsd-license.php) */

/* Windows (Microsoft visual studio) implementation of the call_stack class. */
#ifdef _WIN32 // also defined in 64bit

#include "call_stack.hpp"
#include "../externals/StackWalker/StackWalker.h"
#include <concrt.h>

namespace {

/** Adapter class to interfaces with the StackWalker project.
 *  Source: http://stackwalker.codeplex.com/ */
class StackWalkerAdapter : public StackWalker {
public:
    StackWalkerAdapter(int options) : StackWalker(options) {}
    std::vector<void*> trace;       ///< stack pointers (offsets)
	static std::vector<CallstackEntry> entries;

protected:
    virtual void OnCallstackEntry (CallstackEntryType eType, CallstackEntry &entry) {
        if ( (eType == lastEntry) || (entry.offset == 0) )
            return /*FALSE*/;
        trace.push_back((void*)entry.offset);
		entries.push_back(entry);
        return /*TRUE*/;
    }
    virtual void OnOutput (LPCSTR /*szText*/) {
        // discard (should never be called)
    }
    virtual void OnSymInit (LPCSTR /*szSearchPath*/, DWORD /*symOptions*/, LPCSTR /*szUserName*/) {
        // discard
    }
    virtual void OnLoadModule (LPCSTR /*img*/, LPCSTR /*mod*/, DWORD64 /*baseAddr*/, DWORD /*size*/, DWORD /*result*/, LPCSTR /*symType*/, LPCSTR /*pdbName*/, ULONGLONG /*fileVersion*/) {
        // discard
    }
    virtual void OnDbgHelpErr (LPCSTR /*szFuncName*/, DWORD /*gle*/, DWORD64 /*addr*/) {
        // discard
    }

public:
	::stacktrace::entry lookupEntry(void *addr) {
		CallstackEntry entry;
		entry.offset = (DWORD64)addr;

		for (int i = 0; i < entries.size(); ++i) // GetSymbols(entry)
			if (entries[i].offset == entry.offset)
				entry = entries[i];

		::stacktrace::entry e;
		e.file = entry.lineFileName;
		e.line = entry.lineNumber;
		e.function = entry.name;
		return e;
	}
};

/**
* StackWalker is not thread safe (because DebugHlp is not thread safe); hence,
* guard access by a mutex. And it is slow to initialise, according to the
* documentation; hence, use static objects. */
static StackWalkerAdapter sw_unwinder(StackWalker::RetrieveNone);
static Concurrency::reader_writer_lock         sw_mutex;
std::vector<StackWalker::CallstackEntry> StackWalkerAdapter::entries;
}

namespace stacktrace {

// windows 32 & 64 bit impl.
call_stack::call_stack() {
    Concurrency::reader_writer_lock::scoped_lock lock(sw_mutex);
    sw_unwinder.ShowCallstack();
    trace.swap(sw_unwinder.trace);
}

std::vector<entry> call_stack::resolve_stack() const {
    std::vector<entry> stack;
    Concurrency::reader_writer_lock::scoped_lock lock(sw_mutex);
    StackWalkerAdapter sw_resolver(~StackWalker::SymUseSymSrv);
    for (size_t i=0; i<trace.size(); i++) {
        stacktrace::entry e = sw_resolver.lookupEntry(trace[i]);
        if (e.file.empty())
            continue;
        stack.push_back(e);
    }
	sw_resolver.entries.clear();
    return stack;
}

call_stack::~call_stack () throw() {
    // automatic cleanup
}

} // namespace stacktrace

#endif // _WIN32
