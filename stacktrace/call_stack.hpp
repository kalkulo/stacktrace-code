/* Copyright (c) 2009, Fredrik Orderud
   License: BSD licence (http://www.opensource.org/licenses/bsd-license.php) */

#pragma once
#include <string>
#include <vector>
#include <sstream>

#define CALL_STACK_MAJOR 0
#define CALL_STACK_MINOR 1
#define CALL_STACK_PATCH 0

#define CALL_STACK_VERSION CALL_STACK_MAJOR.CALL_STACK_MINOR.CALL_STACK_PATCH
#define CALL_STACK_VERSION_INT (1000000*CALL_STACK_MAJOR+1000*CALL_STACK_MINOR+CALL_STACK_PATCH)

/* Proper declspec on windows. */
#ifdef DLL_STATE
#    undef DLL_STATE
#endif
#if defined(_MSC_VER) || defined(__CYGWIN__) || defined(__MINGW32__) || defined( __BCPLUSPLUS__)  || defined( __MWERKS__)
#    if MAKE_DLL_stacktrace == 1
#        define DLL_STATE __declspec(dllexport)
#    else
#        define DLL_STATE __declspec(dllimport)
#    endif
#else
#    define DLL_STATE
#endif

namespace stacktrace {

/** Forward declaration */
class call_stack;

/** Call-stack entry datastructure. */
struct entry {
    std::string file;     ///< filename
    size_t      line;     ///< line number
    std::string function; ///< name of function or method

    /** Serialize entry into a text string. */
    std::string to_string() const {
        std::ostringstream os;
        if (line > 0)
            os << function << " at " << file << ":" << line;
        else
            os << function << " in " << file;
        return os.str();
    }
};

/** Stack-trace base class, for retrieving the current call-stack. */
class DLL_STATE call_stack {
public:
    /** Stack-trace constructor. */
    call_stack();

    virtual ~call_stack() throw();

    /** Serializes the call-stack into a text string. See get_stack() for an
        explanation of the parameters. */
    virtual std::string to_string(const char *skipToSymbol="stacktrace::", bool skipOverSymbol=true) const {
        std::ostringstream os;
        std::vector<entry> stack = get_stack(skipToSymbol, skipOverSymbol);
        for (size_t i=0; i < stack.size(); i++)
            os << '[' << i << "] " << stack[i].to_string() << std::endl;
        return os.str();
    }

    /** Return the call-stack above stacktrace::* (by default). If skipToSymbol
        is not NULL, inner frames are skipped until the prefix is found (on the
        symbol). If skipOverSymbol is true, the frames matching the prefix are
        also skipped. */
    std::vector<entry> get_stack(const char* skipToSymbol="stacktrace::", bool skipOverSymbol=true) const {
        std::vector<entry> stack = resolve_stack();
        std::ostringstream os;
        if (skipToSymbol) {
            size_t idx = 0;
            for (; idx < stack.size(); idx++)
                if (stack[idx].function.find(skipToSymbol) != std::string::npos)
                    break;
            if (skipOverSymbol)
                for (; idx < stack.size(); idx++)
                    if (stack[idx].function.find(skipToSymbol) == std::string::npos)
                        break;
            if (idx > 0 && idx < stack.size()) {
                // skipToSymbol is found. If it isn't found (optimized out?), return
                // the whole stack.
                stack.erase(stack.begin(), stack.begin()+idx);
            }
        }
        return stack;
    }
private:
    /** Call stack, unresolved. */
    std::vector<void*> trace;

    /** Method to return resolved stack entries */
    std::vector<entry> resolve_stack() const;

};

} // namespace stacktrace
